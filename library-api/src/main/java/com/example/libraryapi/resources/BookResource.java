package com.example.libraryapi.resources;

import com.example.libraryapi.models.Book;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("/books")
public class BookResource {
    private static List<Book> books = new ArrayList<>();

    static {
        // Simulated data
        books.add(new Book(1, "Book One", "Author One", "Category One"));
        books.add(new Book(2, "Book Two", "Author Two", "Category Two"));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Book> getBooks() {
        return books;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Book addBook(Book book) {
        book.setId(books.size() + 1);
        books.add(book);
        return book;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Book getBook(@PathParam("id") int id) {
        return books.stream().filter(b -> b.getId() == id).findFirst().orElse(null);
    }
}
